//
//  Created by snowman
//  Copyright 2015. All rights reserved.
//


#import "Namespace.XniGame.h"

@interface Rectangle (Extensions)

- (BOOL) containsVector:(Vector2*) value;

@end
