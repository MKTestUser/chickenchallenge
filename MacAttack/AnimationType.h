//
//  Created by snowman
//  Copyright 2015. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
	AnimationFlappingWings,
    AnimationEasterEgg,
	AnimationStage1,
	AnimationStage2,
    BoomChicken,
    BoomUnicorn,
    BoomAirplane,
    BoomAlien,
    AnimationForceField,
    IntroAnimation,
    AnimationEggDead,
    AnimationForceFieldEaster
} AnimationType; 


