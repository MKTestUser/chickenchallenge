//
//  Created by snowman
//  Copyright 2015. All rights reserved.
//


// iRATE
@class iRate;


// ENEMIES
@class Rat;
@class Unicorn;
@class Enemy;
@class Airplane;
@class Alien;

// POWER UP
@class PowerUpContent;
@class PowerUp, PowerUpFactory;
@class GoldenEggPowerUp;
@class SlowedPowerUp;
@class DeadPowerUp;

// GAME CENTER
@protocol GameKitHelper;


// LEVELS
@class Level;
@class Level1;

// GAME
@class Igra;
@class Gameplay;

// PROTOCOL
@protocol IPosition, IKillable, IMovable, IPowerUp;
@protocol IScene;
@protocol ITouchable;

// GRAPHICS
@class AnimationContent;
@class Sprite;
@class AnimatedSprite;
@class AnimatedSpriteFrame;
@class GameRenderer;
@class GraphicsResource, Texture, Texture2D, Effect, EffectTechnique, EffectPass, BasicEffect, DirectionalLight;
@class Rectangle, Color, Vector2;

// SCENE
@class Scene;
@class DrawableGameComponent;
@class SimpleScene;

// SOUND
@class SoundEngine;

// GUI
@class Label, Image;
@class GuiRenderer;
@class SpriteFont;
@class Button;
@class Label;
@class Menu;
@class MirageEnums;
@class ISceneUser;
@class Image;
@class TouchPanelHelper;
@class MainMenu;

// HUD, MENU
@class About;
@class Options;
@class GameHud;
@class GameOver;


// OTHER
@class Random;
@class Lifetime;
@class Animations;
