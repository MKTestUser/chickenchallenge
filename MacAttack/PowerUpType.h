//
//  PowerUpType.h
//  Breakout
//
//  Created by Matej Jan on 23.11.10.
//  Copyright 2010 Retronator. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef enum {
	PowerUpTypeGoldenEgg,
    PowerUpTypeSlow,
	PowerUpTypeDeath,
    PowerUpTypes
} PowerUpType; //PowerUpType
