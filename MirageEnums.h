//
//  MirageEnums.h
//  Express
//
//  Created by Matej Jan on 23.11.10.
//  Copyright 2010 Retronator. All rights reserved.
//

typedef enum {
	HorizontalAlignCustom,
	HorizontalAlignLeft,
	HorizontalAlignCenter,
	HorizontalAlignRight
} HorizontalAlign;

typedef enum {
	VerticalAlignCustom,
	VerticalAlignTop,
	VerticalAlignMiddle,
	VerticalAlignBottom
} VerticalAlign;


